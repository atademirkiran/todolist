import React from 'react';

const Card = (props) => {

    return (
        <div className="card">
        <img className="card-img-top" src={props.image} ></img>
        <div className="card-body">
        <h5 className="card-title">{props.cardTitle}</h5>
        <p className="card-text">{props.cardText}</p>
        <a href="#" className="btn btn-primary">{props.cardPrice}</a>
        </div>
    </div>
    );

};

export default Card;